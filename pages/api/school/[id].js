
export default function handler(req, res) {
    const schools = [
      {
        id: 1,
        url: "https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/Interior_SMAK_Frateran.jpeg/450px-Interior_SMAK_Frateran.jpeg",
        RegistrationDate: new Date("2023-12-01"),
        name: `SMAK Frateran Surabaya`,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. At
            auctor urna nunc id cursus metus. Libero nunc consequat interdum
            varius sit amet mattis vulputate enim. Risus nec feugiat in
            fermentum posuere urna nec. Elit sed vulputate mi sit amet mauris
            commodo quis imperdiet. In egestas erat imperdiet sed euismod nisi
            porta lorem mollis. Tristique nulla aliquet enim tortor at auctor
            urna nunc id. Aliquet risus feugiat in ante metus dictum at tempor.
            Et malesuada fames ac turpis egestas integer eget aliquet. Tristique
            senectus et netus et. Odio morbi quis commodo odio aenean. Faucibus
            in ornare quam viverra orci sagittis eu volutpat. Quam viverra orci
            sagittis eu volutpat odio. Dolor purus non enim praesent elementum
            facilisis leo. Enim ut tellus elementum sagittis vitae et leo. Urna
            nunc id cursus metus. Viverra nibh cras pulvinar mattis. Elit
            pellentesque habitant morbi tristique senectus et netus et
            malesuada. Vulputate ut pharetra sit amet aliquam id diam maecenas
            ultricies. Pretium quam vulputate dignissim suspendisse in est ante
            in. Iaculis eu non diam phasellus vestibulum. Elit scelerisque
            mauris pellentesque pulvinar pellentesque habitant morbi tristique
            senectus. Mi proin sed libero enim sed faucibus turpis in. Cursus
            eget nunc scelerisque viverra mauris in aliquam sem. Vestibulum
            morbi blandit cursus risus at ultrices mi tempus imperdiet. Nulla
            facilisi cras fermentum odio. Tortor pretium viverra suspendisse
            potenti. In hendrerit gravida rutrum quisque non tellus orci.
            Habitasse platea dictumst vestibulum rhoncus est pellentesque elit.
            Eleifend mi in nulla posuere sollicitudin aliquam. Massa ultricies
            mi quis hendrerit dolor magna eget. Nibh mauris cursus mattis
            molestie a iaculis. Commodo ullamcorper a lacus vestibulum sed arcu
            non odio. Montes nascetur ridiculus mus mauris vitae ultricies leo
            integer malesuada. Nullam ac tortor vitae purus faucibus ornare
            suspendisse sed nisi. Gravida arcu ac tortor dignissim convallis.
            Volutpat lacus laoreet non curabitur. Potenti nullam ac tortor vitae
            purus. Facilisis mauris sit amet massa vitae tortor condimentum
            lacinia quis. Ut diam quam nulla porttitor massa. Magna ac placerat
            vestibulum lectus mauris. Nisl nunc mi ipsum faucibus vitae aliquet
            nec. Blandit volutpat maecenas volutpat blandit aliquam etiam. Neque
            vitae tempus quam pellentesque nec. Adipiscing enim eu turpis
            egestas pretium aenean. Quam quisque id diam vel quam elementum.
            Semper eget duis at tellus at. Ac turpis egestas sed tempus urna et.
            Duis at tellus at urna condimentum mattis pellentesque id. Sit amet
            purus gravida quis. Sit amet porttitor eget dolor morbi non.
            Pulvinar neque laoreet suspendisse interdum. Hac habitasse platea
            dictumst quisque sagittis. Quam nulla porttitor massa id neque.
            Morbi tempus iaculis urna id. Id neque aliquam vestibulum morbi
            blandit cursus risus at. Amet consectetur adipiscing elit ut
            aliquam. Euismod lacinia at quis risus sed vulputate odio ut enim.
            Consectetur a erat nam at lectus urna duis convallis convallis.`,
        curriculum: `
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. At
            auctor urna nunc id cursus metus. Libero nunc consequat interdum
            varius sit amet mattis vulputate enim. Risus nec feugiat in
            fermentum posuere urna nec. Elit sed vulputate mi sit amet mauris
            commodo quis imperdiet. In egestas erat imperdiet sed euismod nisi
            porta lorem mollis. Tristique nulla aliquet enim tortor at auctor
            urna nunc id. Aliquet risus feugiat in ante metus dictum at tempor.`,
      },
      {
        id: 2,
        url: "http://www.mardiwiyata.org/images/sekolah/1566786759194452650214170143861875903220103125108718117169752092404168.png",
        RegistrationDate: new Date("2023-12-01"),
        name: `SMPK Angelus Custos`,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. At
            auctor urna nunc id cursus metus. Libero nunc consequat interdum
            varius sit amet mattis vulputate enim. Risus nec feugiat in
            fermentum posuere urna nec. Elit sed vulputate mi sit amet mauris
            commodo quis imperdiet. In egestas erat imperdiet sed euismod nisi
            porta lorem mollis. Tristique nulla aliquet enim tortor at auctor
            urna nunc id. Aliquet risus feugiat in ante metus dictum at tempor.
            Et malesuada fames ac turpis egestas integer eget aliquet. Tristique
            senectus et netus et. Odio morbi quis commodo odio aenean. Faucibus
            in ornare quam viverra orci sagittis eu volutpat. Quam viverra orci
            sagittis eu volutpat odio. Dolor purus non enim praesent elementum
            facilisis leo. Enim ut tellus elementum sagittis vitae et leo. Urna
            nunc id cursus metus. Viverra nibh cras pulvinar mattis. Elit
            pellentesque habitant morbi tristique senectus et netus et
            malesuada. Vulputate ut pharetra sit amet aliquam id diam maecenas
            ultricies. Pretium quam vulputate dignissim suspendisse in est ante
            in. Iaculis eu non diam phasellus vestibulum. Elit scelerisque
            mauris pellentesque pulvinar pellentesque habitant morbi tristique
            senectus. Mi proin sed libero enim sed faucibus turpis in. Cursus
            eget nunc scelerisque viverra mauris in aliquam sem. Vestibulum
            morbi blandit cursus risus at ultrices mi tempus imperdiet. Nulla
            facilisi cras fermentum odio. Tortor pretium viverra suspendisse
            potenti. In hendrerit gravida rutrum quisque non tellus orci.
            Habitasse platea dictumst vestibulum rhoncus est pellentesque elit.
            Eleifend mi in nulla posuere sollicitudin aliquam. Massa ultricies
            mi quis hendrerit dolor magna eget. Nibh mauris cursus mattis
            molestie a iaculis. Commodo ullamcorper a lacus vestibulum sed arcu
            non odio. Montes nascetur ridiculus mus mauris vitae ultricies leo
            integer malesuada. Nullam ac tortor vitae purus faucibus ornare
            suspendisse sed nisi. Gravida arcu ac tortor dignissim convallis.
            Volutpat lacus laoreet non curabitur. Potenti nullam ac tortor vitae
            purus. Facilisis mauris sit amet massa vitae tortor condimentum
            lacinia quis. Ut diam quam nulla porttitor massa. Magna ac placerat
            vestibulum lectus mauris. Nisl nunc mi ipsum faucibus vitae aliquet
            nec. Blandit volutpat maecenas volutpat blandit aliquam etiam. Neque
            vitae tempus quam pellentesque nec. Adipiscing enim eu turpis
            egestas pretium aenean. Quam quisque id diam vel quam elementum.
            Semper eget duis at tellus at. Ac turpis egestas sed tempus urna et.
            Duis at tellus at urna condimentum mattis pellentesque id. Sit amet
            purus gravida quis. Sit amet porttitor eget dolor morbi non.
            Pulvinar neque laoreet suspendisse interdum. Hac habitasse platea
            dictumst quisque sagittis. Quam nulla porttitor massa id neque.
            Morbi tempus iaculis urna id. Id neque aliquam vestibulum morbi
            blandit cursus risus at. Amet consectetur adipiscing elit ut
            aliquam. Euismod lacinia at quis risus sed vulputate odio ut enim.
            Consectetur a erat nam at lectus urna duis convallis convallis.`,
        curriculum: `
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. At
            auctor urna nunc id cursus metus. Libero nunc consequat interdum
            varius sit amet mattis vulputate enim. Risus nec feugiat in
            fermentum posuere urna nec. Elit sed vulputate mi sit amet mauris
            commodo quis imperdiet. In egestas erat imperdiet sed euismod nisi
            porta lorem mollis. Tristique nulla aliquet enim tortor at auctor
            urna nunc id. Aliquet risus feugiat in ante metus dictum at tempor.`,
      },
      {
        id: 3,
        url: "http://www.mardiwiyata.org/images/sekolah/156686980916551747423619493308847832469046123712578827281476958719.jpg",
        RegistrationDate: new Date("2023-12-01"),
        name: `SDK St. Xaverius`,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. At
            auctor urna nunc id cursus metus. Libero nunc consequat interdum
            varius sit amet mattis vulputate enim. Risus nec feugiat in
            fermentum posuere urna nec. Elit sed vulputate mi sit amet mauris
            commodo quis imperdiet. In egestas erat imperdiet sed euismod nisi
            porta lorem mollis. Tristique nulla aliquet enim tortor at auctor
            urna nunc id. Aliquet risus feugiat in ante metus dictum at tempor.
            Et malesuada fames ac turpis egestas integer eget aliquet. Tristique
            senectus et netus et. Odio morbi quis commodo odio aenean. Faucibus
            in ornare quam viverra orci sagittis eu volutpat. Quam viverra orci
            sagittis eu volutpat odio. Dolor purus non enim praesent elementum
            facilisis leo. Enim ut tellus elementum sagittis vitae et leo. Urna
            nunc id cursus metus. Viverra nibh cras pulvinar mattis. Elit
            pellentesque habitant morbi tristique senectus et netus et
            malesuada. Vulputate ut pharetra sit amet aliquam id diam maecenas
            ultricies. Pretium quam vulputate dignissim suspendisse in est ante
            in. Iaculis eu non diam phasellus vestibulum. Elit scelerisque
            mauris pellentesque pulvinar pellentesque habitant morbi tristique
            senectus. Mi proin sed libero enim sed faucibus turpis in. Cursus
            eget nunc scelerisque viverra mauris in aliquam sem. Vestibulum
            morbi blandit cursus risus at ultrices mi tempus imperdiet. Nulla
            facilisi cras fermentum odio. Tortor pretium viverra suspendisse
            potenti. In hendrerit gravida rutrum quisque non tellus orci.
            Habitasse platea dictumst vestibulum rhoncus est pellentesque elit.
            Eleifend mi in nulla posuere sollicitudin aliquam. Massa ultricies
            mi quis hendrerit dolor magna eget. Nibh mauris cursus mattis
            molestie a iaculis. Commodo ullamcorper a lacus vestibulum sed arcu
            non odio. Montes nascetur ridiculus mus mauris vitae ultricies leo
            integer malesuada. Nullam ac tortor vitae purus faucibus ornare
            suspendisse sed nisi. Gravida arcu ac tortor dignissim convallis.
            Volutpat lacus laoreet non curabitur. Potenti nullam ac tortor vitae
            purus. Facilisis mauris sit amet massa vitae tortor condimentum
            lacinia quis. Ut diam quam nulla porttitor massa. Magna ac placerat
            vestibulum lectus mauris. Nisl nunc mi ipsum faucibus vitae aliquet
            nec. Blandit volutpat maecenas volutpat blandit aliquam etiam. Neque
            vitae tempus quam pellentesque nec. Adipiscing enim eu turpis
            egestas pretium aenean. Quam quisque id diam vel quam elementum.
            Semper eget duis at tellus at. Ac turpis egestas sed tempus urna et.
            Duis at tellus at urna condimentum mattis pellentesque id. Sit amet
            purus gravida quis. Sit amet porttitor eget dolor morbi non.
            Pulvinar neque laoreet suspendisse interdum. Hac habitasse platea
            dictumst quisque sagittis. Quam nulla porttitor massa id neque.
            Morbi tempus iaculis urna id. Id neque aliquam vestibulum morbi
            blandit cursus risus at. Amet consectetur adipiscing elit ut
            aliquam. Euismod lacinia at quis risus sed vulputate odio ut enim.
            Consectetur a erat nam at lectus urna duis convallis convallis.`,
        curriculum: `
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. At
            auctor urna nunc id cursus metus. Libero nunc consequat interdum
            varius sit amet mattis vulputate enim. Risus nec feugiat in
            fermentum posuere urna nec. Elit sed vulputate mi sit amet mauris
            commodo quis imperdiet. In egestas erat imperdiet sed euismod nisi
            porta lorem mollis. Tristique nulla aliquet enim tortor at auctor
            urna nunc id. Aliquet risus feugiat in ante metus dictum at tempor.`,
      },
      {
        id: 4,
        url: "https://asset.kompas.com/crops/8V62kGehA9zuJTIM6UaGm9ksd7s=/180x43:1242x751/750x500/data/photo/2022/09/02/6311818aaf0a3.jpg",
        RegistrationDate: new Date("2023-12-01"),
        name: `SMAK St. Louis 1`,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. At
            auctor urna nunc id cursus metus. Libero nunc consequat interdum
            varius sit amet mattis vulputate enim. Risus nec feugiat in
            fermentum posuere urna nec. Elit sed vulputate mi sit amet mauris
            commodo quis imperdiet. In egestas erat imperdiet sed euismod nisi
            porta lorem mollis. Tristique nulla aliquet enim tortor at auctor
            urna nunc id. Aliquet risus feugiat in ante metus dictum at tempor.
            Et malesuada fames ac turpis egestas integer eget aliquet. Tristique
            senectus et netus et. Odio morbi quis commodo odio aenean. Faucibus
            in ornare quam viverra orci sagittis eu volutpat. Quam viverra orci
            sagittis eu volutpat odio. Dolor purus non enim praesent elementum
            facilisis leo. Enim ut tellus elementum sagittis vitae et leo. Urna
            nunc id cursus metus. Viverra nibh cras pulvinar mattis. Elit
            pellentesque habitant morbi tristique senectus et netus et
            malesuada. Vulputate ut pharetra sit amet aliquam id diam maecenas
            ultricies. Pretium quam vulputate dignissim suspendisse in est ante
            in. Iaculis eu non diam phasellus vestibulum. Elit scelerisque
            mauris pellentesque pulvinar pellentesque habitant morbi tristique
            senectus. Mi proin sed libero enim sed faucibus turpis in. Cursus
            eget nunc scelerisque viverra mauris in aliquam sem. Vestibulum
            morbi blandit cursus risus at ultrices mi tempus imperdiet. Nulla
            facilisi cras fermentum odio. Tortor pretium viverra suspendisse
            potenti. In hendrerit gravida rutrum quisque non tellus orci.
            Habitasse platea dictumst vestibulum rhoncus est pellentesque elit.
            Eleifend mi in nulla posuere sollicitudin aliquam. Massa ultricies
            mi quis hendrerit dolor magna eget. Nibh mauris cursus mattis
            molestie a iaculis. Commodo ullamcorper a lacus vestibulum sed arcu
            non odio. Montes nascetur ridiculus mus mauris vitae ultricies leo
            integer malesuada. Nullam ac tortor vitae purus faucibus ornare
            suspendisse sed nisi. Gravida arcu ac tortor dignissim convallis.
            Volutpat lacus laoreet non curabitur. Potenti nullam ac tortor vitae
            purus. Facilisis mauris sit amet massa vitae tortor condimentum
            lacinia quis. Ut diam quam nulla porttitor massa. Magna ac placerat
            vestibulum lectus mauris. Nisl nunc mi ipsum faucibus vitae aliquet
            nec. Blandit volutpat maecenas volutpat blandit aliquam etiam. Neque
            vitae tempus quam pellentesque nec. Adipiscing enim eu turpis
            egestas pretium aenean. Quam quisque id diam vel quam elementum.
            Semper eget duis at tellus at. Ac turpis egestas sed tempus urna et.
            Duis at tellus at urna condimentum mattis pellentesque id. Sit amet
            purus gravida quis. Sit amet porttitor eget dolor morbi non.
            Pulvinar neque laoreet suspendisse interdum. Hac habitasse platea
            dictumst quisque sagittis. Quam nulla porttitor massa id neque.
            Morbi tempus iaculis urna id. Id neque aliquam vestibulum morbi
            blandit cursus risus at. Amet consectetur adipiscing elit ut
            aliquam. Euismod lacinia at quis risus sed vulputate odio ut enim.
            Consectetur a erat nam at lectus urna duis convallis convallis.`,
        curriculum: `
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. At
            auctor urna nunc id cursus metus. Libero nunc consequat interdum
            varius sit amet mattis vulputate enim. Risus nec feugiat in
            fermentum posuere urna nec. Elit sed vulputate mi sit amet mauris
            commodo quis imperdiet. In egestas erat imperdiet sed euismod nisi
            porta lorem mollis. Tristique nulla aliquet enim tortor at auctor
            urna nunc id. Aliquet risus feugiat in ante metus dictum at tempor.`,
      },
      {
        id: 5,
        url: "https://www.apple.com/newsroom/images/product/mac/standard/Apple_MacBook-Pro_16-inch-Screen_10182021_big_carousel.jpg.large.jpg",
        RegistrationDate: new Date("2023-12-01"),
        name: `SMA Kr. Gloria`,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. At
            auctor urna nunc id cursus metus. Libero nunc consequat interdum
            varius sit amet mattis vulputate enim. Risus nec feugiat in
            fermentum posuere urna nec. Elit sed vulputate mi sit amet mauris
            commodo quis imperdiet. In egestas erat imperdiet sed euismod nisi
            porta lorem mollis. Tristique nulla aliquet enim tortor at auctor
            urna nunc id. Aliquet risus feugiat in ante metus dictum at tempor.
            Et malesuada fames ac turpis egestas integer eget aliquet. Tristique
            senectus et netus et. Odio morbi quis commodo odio aenean. Faucibus
            in ornare quam viverra orci sagittis eu volutpat. Quam viverra orci
            sagittis eu volutpat odio. Dolor purus non enim praesent elementum
            facilisis leo. Enim ut tellus elementum sagittis vitae et leo. Urna
            nunc id cursus metus. Viverra nibh cras pulvinar mattis. Elit
            pellentesque habitant morbi tristique senectus et netus et
            malesuada. Vulputate ut pharetra sit amet aliquam id diam maecenas
            ultricies. Pretium quam vulputate dignissim suspendisse in est ante
            in. Iaculis eu non diam phasellus vestibulum. Elit scelerisque
            mauris pellentesque pulvinar pellentesque habitant morbi tristique
            senectus. Mi proin sed libero enim sed faucibus turpis in. Cursus
            eget nunc scelerisque viverra mauris in aliquam sem. Vestibulum
            morbi blandit cursus risus at ultrices mi tempus imperdiet. Nulla
            facilisi cras fermentum odio. Tortor pretium viverra suspendisse
            potenti. In hendrerit gravida rutrum quisque non tellus orci.
            Habitasse platea dictumst vestibulum rhoncus est pellentesque elit.
            Eleifend mi in nulla posuere sollicitudin aliquam. Massa ultricies
            mi quis hendrerit dolor magna eget. Nibh mauris cursus mattis
            molestie a iaculis. Commodo ullamcorper a lacus vestibulum sed arcu
            non odio. Montes nascetur ridiculus mus mauris vitae ultricies leo
            integer malesuada. Nullam ac tortor vitae purus faucibus ornare
            suspendisse sed nisi. Gravida arcu ac tortor dignissim convallis.
            Volutpat lacus laoreet non curabitur. Potenti nullam ac tortor vitae
            purus. Facilisis mauris sit amet massa vitae tortor condimentum
            lacinia quis. Ut diam quam nulla porttitor massa. Magna ac placerat
            vestibulum lectus mauris. Nisl nunc mi ipsum faucibus vitae aliquet
            nec. Blandit volutpat maecenas volutpat blandit aliquam etiam. Neque
            vitae tempus quam pellentesque nec. Adipiscing enim eu turpis
            egestas pretium aenean. Quam quisque id diam vel quam elementum.
            Semper eget duis at tellus at. Ac turpis egestas sed tempus urna et.
            Duis at tellus at urna condimentum mattis pellentesque id. Sit amet
            purus gravida quis. Sit amet porttitor eget dolor morbi non.
            Pulvinar neque laoreet suspendisse interdum. Hac habitasse platea
            dictumst quisque sagittis. Quam nulla porttitor massa id neque.
            Morbi tempus iaculis urna id. Id neque aliquam vestibulum morbi
            blandit cursus risus at. Amet consectetur adipiscing elit ut
            aliquam. Euismod lacinia at quis risus sed vulputate odio ut enim.
            Consectetur a erat nam at lectus urna duis convallis convallis.`,
        curriculum: `
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. At
            auctor urna nunc id cursus metus. Libero nunc consequat interdum
            varius sit amet mattis vulputate enim. Risus nec feugiat in
            fermentum posuere urna nec. Elit sed vulputate mi sit amet mauris
            commodo quis imperdiet. In egestas erat imperdiet sed euismod nisi
            porta lorem mollis. Tristique nulla aliquet enim tortor at auctor
            urna nunc id. Aliquet risus feugiat in ante metus dictum at tempor.`,
      },
    ];
    let id = req.query.id;
    let data = schools.filter(e => e.id == id);
    if(data){
        data = data[0];
        res.status(200).json({
            status: 200,
            data: data
        });
    }
    else{
        res.status(400).json({
            status: 400,
            message: 'No data found'
        })
    }
}

import Router from "next/router";
import { useState } from "react";

export default function Navbar() {
  const [keyword, setKeyword] = useState("");
  return (
    <>
      <nav className="w-full flex py-4 px-4 items-center content-center">
        <a href="/" className="p-3">
          <h1 className="text-3xl text-blue-400">
            <strong>Sekolahan</strong>
          </h1>
        </a>
        <div className="flex-grow flex">
          <div className="flex-grow items-center"></div>
          <div>
            {/* <input type="text" className="border-2 border-black p-1" placeholder="Cari sekolah..."/> */}
            <div class="relative text-gray-600 focus-within:text-gray-400 hidden md:block">
              <span class="absolute inset-y-0 left-0 flex items-center pl-2">
                <button
                  type="submit"
                  class="p-1 focus:outline-none focus:shadow-outline"
                >
                  <svg
                    fill="none"
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    viewBox="0 0 24 24"
                    class="w-6 h-6"
                  >
                    <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
                  </svg>
                </button>
              </span>
              <input
                type="search"
                name="q"
                class="py-2 text-sm text-gray-900 rounded-md pl-10 focus:outline-none focus:bg-white focus:text-gray-900 border-2 w-72"
                placeholder="Search..."
                autocomplete="off"
                value={keyword}
                onChange={(e) => { setKeyword(e.target.value) } }
                onKeyUp={(e) => {if(e.code == "Enter") { window.history.pushState(null, null, `/?search=${keyword}`);;window.location.reload(true);}}}
              />
            </div>
          </div>
        </div>
      </nav>
    </>
  );
}

import { BiDetail } from "react-icons/bi";
import { BsWhatsapp } from "react-icons/bs";

export default function SchoolCard(school) {
  return (
    <div className="mx-4 mt-2">
      <div class="max-w-xs rounded-md overflow-hidden shadow-lg hover:scale-105 transition duration-500 cursor-pointer">
        <div>
          <img
            //   src="https://www.apple.com/newsroom/images/product/mac/standard/Apple_MacBook-Pro_16-inch-Screen_10182021_big_carousel.jpg.large.jpg"
            //   alt=""
            src={school.url}
            alt=""
          />
        </div>
        <div class="py-4 px-4 bg-white">
          <h3 class="text-md font-semibold text-gray-600">{school.name}</h3>
          {/* <p class="mt-4 text-lg font-thin">$ 2400</p> */}
          <a href={`/detail/${school.id}`}>
            <span class="flex items-center justify-center mt-4 w-full bg-yellow-400 hover:bg-yellow-500 py-1 rounded">
              <BiDetail /> &nbsp;
              <button class="font-semibold text-gray-800">Lihat Detail</button>
            </span>
          </a>
          <span class="flex items-center justify-center mt-4 w-full bg-green-400 hover:bg-green-500 py-1 rounded">
            <BsWhatsapp /> &nbsp;
            <button class="font-semibold text-gray-800">Kontak Kami</button>
          </span>
        </div>
      </div>
    </div>
  );
}

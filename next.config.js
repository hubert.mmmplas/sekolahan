/** @type {import('next').NextConfig} */
const nextConfig = {
  basePath: '/sekolah2' ,
  reactStrictMode: true,
  images:{
    unoptimized: true
  }
}

module.exports = nextConfig


// const withImages = require('next-images');

// const redirects = {
//   async redirects() {
//     return [
//       {
//         source: '/dashboards',
//         destination: '/dashboards/tasks',
//         permanent: true
//       }
//     ];
//   }
// };

// module.exports = withImages(redirects);